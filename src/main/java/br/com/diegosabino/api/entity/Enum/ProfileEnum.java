package br.com.diegosabino.api.entity.Enum;

public enum ProfileEnum {
	
	ROLE_ADMIN,
	ROLE_CUSTOMER,
	ROLE_TECHNICIAN;

}
