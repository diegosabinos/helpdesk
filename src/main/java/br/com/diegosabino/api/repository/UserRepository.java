package br.com.diegosabino.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.diegosabino.api.entity.User;

public interface UserRepository extends MongoRepository<User, String>{

	User findByEmail(String email);
}
