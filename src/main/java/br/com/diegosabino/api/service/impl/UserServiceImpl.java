package br.com.diegosabino.api.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.jaxb.SpringDataJaxb;
import org.springframework.stereotype.Component;

import br.com.diegosabino.api.entity.User;
import br.com.diegosabino.api.repository.UserRepository;
import br.com.diegosabino.api.service.UserService;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    @Override
    public User createOrUpdate(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public User findById(String id) {
        return this.userRepository.findOne(id);
    }

    @Override
    public void delete(String id) {
        this.userRepository.delete(id);

    }

    @Override
    public Page<User> findAll(Integer page, Integer count) {
        Pageable pages = new PageRequest(page, count);
        return this.userRepository.findAll(pages);
    }

}
