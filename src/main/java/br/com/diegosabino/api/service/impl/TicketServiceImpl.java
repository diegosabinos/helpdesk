package br.com.diegosabino.api.service.impl;

import br.com.diegosabino.api.entity.ChangeStatus;
import br.com.diegosabino.api.entity.Ticket;
import br.com.diegosabino.api.repository.ChangeStatusRepository;
import br.com.diegosabino.api.repository.TicketRepository;
import br.com.diegosabino.api.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private ChangeStatusRepository changeStatusRepository;


    @Override
    public Ticket createOrUpdate(Ticket ticket) {
        return this.ticketRepository.save(ticket);
    }

    @Override
    public Ticket findById(String id) {
        return this.ticketRepository.findOne(id);
    }

    @Override
    public void delete(String id) {
        this.ticketRepository.delete(id);
    }

    @Override
    public Page<Ticket> listTicket(int page, int count) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findAll(pages);
    }

    @Override
    public ChangeStatus createOrChangeStatus(ChangeStatus changeStatus) {
        return this.changeStatusRepository.save(changeStatus);
    }

    @Override
    public Iterable<ChangeStatus> listChangeStatus(String ticketId) {
        return this.changeStatusRepository.findByTicketIdOrderByDateChangeStatusDesc(ticketId);
    }

    @Override
    public Page<Ticket> findByCurrentUser(int page, int count, String userID) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByUserIdOrderByDateDesc(pages, userID);
    }

    @Override
    public Page<Ticket> findByParametros(int page, int count, String title, String status, String priority) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingOrderByDateDesc(title, status, priority, pages);
    }

    @Override
    public Page<Ticket> findByParametrosAndCurrentUser(int page, int count, String title, String status, String priority, String userID) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndUserIdOrderByDateDesc(title, status, priority, userID, pages);
    }

    @Override
    public Page<Ticket> findByNumber(int page, int count, Integer number) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByNumber(number, pages);
    }

    @Override
    public Iterable<Ticket> findAll() {
        return this.ticketRepository.findAll();
    }

    @Override
    public Page<Ticket> findByParametrosAndAssignedUser(int page, int count, String title, String status, String priority, String userID) {
        Pageable pages = new PageRequest(page, count);
        return this.ticketRepository.findByTitleIgnoreCaseContainingAndStatusIgnoreCaseContainingAndPriorityIgnoreCaseContainingAndAssignedUserIdOrderByDateDesc(title, status, priority, userID, pages);
    }
}
