package br.com.diegosabino.api.service;

import br.com.diegosabino.api.entity.ChangeStatus;
import br.com.diegosabino.api.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public interface TicketService {

    Ticket createOrUpdate(Ticket ticket);

    Ticket findById(String id);

    void delete(String id);

    Page<Ticket> listTicket(int page, int count);

    ChangeStatus createOrChangeStatus(ChangeStatus changeStatus);

    Iterable<ChangeStatus> listChangeStatus(String ticketId);

    Page<Ticket> findByCurrentUser(int page, int count, String userID);

    Page<Ticket> findByParametros(int page, int count, String title, String status, String priority);

    Page<Ticket> findByParametrosAndCurrentUser(int page, int count, String title, String status, String priority, String userID);

    Page<Ticket> findByNumber(int page, int count, Integer number);

    Iterable<Ticket> findAll();

    Page<Ticket> findByParametrosAndAssignedUser(int page, int count, String title, String status, String priority, String userID);

}
