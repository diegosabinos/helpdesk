package br.com.diegosabino.api.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import br.com.diegosabino.api.entity.User;

@Component
public interface UserService {

	User findByEmail(String email);

	User createOrUpdate(User user);

	User findById(String id);

	void delete(String id);

	Page<User> findAll(Integer page, Integer count);

}
